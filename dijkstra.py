#!/usr/bin/env python3.4

from heap import MinHeap

class Node:
    def __init__(self, nodeid, dist=10**10):
        self.nodeid = nodeid
        self.dist = dist

    def __repr__(self):
        return '<Node {}: {}>'.format(self.nodeid, self.dist)

    @staticmethod
    def setNodeValue(node, value):
        node.dist = value

class Graph:
    def __init__(self):
        self.nodes = {} # {nodeid: Node}
        self.edges = {} # {nodeid: [(distance, nodeid)]}

    def addNode(self, node, links=[]):
        self.nodes[node.nodeid] = node
        self.edges[node.nodeid] = links

    def addEdge(self, nodeid, distance, target):
        self.edges[nodeid].append((distance, target))


class Dijkstra:
    def __init__(self, nodes):
        self.graph = Graph()
        # {nodeid: [(dist, nodeid)]}
        for n, links in nodes.items():
            self.graph.addNode(Node(n), links)

    def shortestPath(self, source):
        Node.setNodeValue(self.graph.nodes[source], 0)
        h = MinHeap(lambda i:i.dist, Node.setNodeValue)
        for node in self.graph.nodes.values():
            h.insert(node)
        for numnodes in range(len(self.graph.nodes)):
            source = h.pop()
            sourcedist = source.dist
            for dist, dest in self.graph.edges[source.nodeid]:
                newdist = sourcedist + dist
                if newdist < self.graph.nodes[dest].dist:
                    h.decreaseKey(self.graph.nodes[dest], newdist)
        return self.graph.nodes

class BellmanFord:
    def __init__(self, nodes):
        self.graph = Graph()
        for n, links in nodes.items():
            self.graph.addNode(Node(n), links)

    def shortestPath(self, source):
        self.graph.nodes[source].dist = 0
        for _ in range(len(self.graph.nodes) - 1):
            for node_u_id, edges in self.graph.edges.items():
                du = self.graph.nodes[node_u_id].dist
                for weight, node_v_id in edges:
                    if self.graph.nodes[node_v_id].dist > du + weight:
                        self.graph.nodes[node_v_id].dist = du + weight
        for node_u_id, edges in self.graph.edges.items():
            du = self.graph.nodes[node_u_id].dist
            for weight, node_v_id in edges:
                if self.graph.nodes[node_v_id].dist > du + weight:
                    self.graph.nodes[node_v_id].dist = None
        return self.graph.nodes
"""
Floyd warshall
let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
for each edge (u,v)
   dist[u][v] ← w(u,v)  // the weight of the edge (u,v)
for each vertex v
   dist[v][v] ← 0
for k from 1 to |V|
   for i from 1 to |V|
      for j from 1 to |V|
         if dist[i][j] > dist[i][k] + dist[k][j] 
             dist[i][j] ← dist[i][k] + dist[k][j]
         end if
"""


if __name__ == '__main__':
    nodes = {
            1: [(3, 3), (6, 4)],
            2: [(1, 4)],
            3: [(5, 4)],
            4: [],
            0: [(2, 1), (6, 2), (7, 3)]
            }

    d = Dijkstra(nodes)
    print(d.shortestPath(0))
    bf = BellmanFord(nodes)
    print(bf.shortestPath(0))
