

def knapsack(items, # (size, value)
             max_size):

    def dp(i, left):  # returns value
        print('called for', i, left)
        if left <= 0 or i == len(items):
            print('left early')
            return 0
        ret = dp(i+1, left)  # excluding
        if left >= items[i][0]:  # including
            ret = max(ret, items[i][1] + dp(i+1, left - items[i][0]))
        return ret
    return dp(0, max_size)

def knapsack_tuple(items, maxsize):
    def dp(items, maxsize):
	n = len(items)
	def dp(i, left):
	    if i == n:
		return 0
	    ret = dp(i + 1, left)  # excluding
	    if dp[i].size <= left:  # including
		including = items[i].value + dp(i + 1, left - items[i].size)
		if including > ret:
		    ret = including
	    return ret
	return dp(0, maxsize)
