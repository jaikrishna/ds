#!/usr/bin/env python3

class Node:
    slots = ('val', 'left', 'right')

    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def __repr__(self):
        return '<Node {}>'.format(self.val)


class BinaryTree:

    def __init__(self, root=None):
        self.root = root

    def insert(self, node):
        if self.root is None:
            self.root = node
            return self.root
        head = self.root
        while True:
            if node.val < head.val:
                if head.left is not None:
                    head = head.left
                else:
                    head.left = node
                    return head.left
            elif node.val > head.val:
                if head.right is  not None:
                    head = head.right
                else:
                    head.right = node
                    return head.right
            else:
                return head

    def search(self, val):
        head = self.root
        while head is not None:
            if val < head.val:
                head = head.left
            elif val > head.val:
                head = head.right
            else:
                return head
        return None

    def remove(self, val):
        node = self.root
        parent = None
        while (node is not None) and (node.val != val):
            parent = node
            if val < node.val:
                node = node.left
            elif val > node.val:
                node = node.right
        if node is None: # not found
            return None

        left_child = (parent is not None) and (parent.left is node)

        if node.left is None:  # no left child
            if parent is None:
                self.root = node.right
            elif left_child:
                parent.left = node.right
            else:
                parent.right = node.right
            node.right = None
            return node
        elif node.right is None:
            if parent is None:
                self.root = node.left
            elif left_child:
                parent.left = node.left
            else:
                parent.right = node.left
            node.left = None
            return node
        else: # both children
            next_inorder = node.right
            next_inorder_parent = None
            while next_inorder.left is not None:
                next_inorder_parent = next_inorder
                next_inorder = next_inorder.left
            node.val, next_inorder.val = next_inorder.val, node.val
            if next_inorder_parent is not None:
                next_inorder_parent.left = next_inorder.right
            else:
                node.right = next_inorder.right
            next_inorder.right = None
            return next_inorder


    def min(self, head):
        while head.left is not None:
            head = head.left
        return head


    def inorder(self): # LNR
        to_trav = []
        head = self.root
        while (head is not None) or to_trav:
            if head is not None:
                to_trav.append(head)
                head = head.left
            else:
                head = to_trav.pop()
                yield head
                head = head.right

    def preorder(self):  # NLR
        to_trav = [self.root] if self.root is not None else []
        while to_trav:
            head = to_trav.pop()
            yield head
            if head.right is not None:
                to_trav.append(head.right)
            if head.left is not None:
                to_trav.append(head.left)

    def postorder(self):  # LRN
        to_trav = [(self.root, False)] if self.root is not None else []
        while to_trav:
            head, expanded = to_trav.pop()
            if not expanded:
                to_trav.append((head, True))
                if head.right is not None:
                    to_trav.append((head.right, False))
                if head.left is not None:
                    to_trav.append((head.left, False))
            else:
                yield head

    def postorder2(self):  # LRN
        to_trav = []
        head = self.root
        last = None  # last visited node
        while to_trav or (head is not None):
            if head is not None:
                to_trav.append(head)
                head = head.left
            else:
                peek = to_trav[-1].right
                # the only condition to go back to a parent is when we just
                # visited the right child
                # not (peek is None or peek is last)
                if peek is not None and peek is not last:
                    head = peek
                else:
                    last = to_trav.pop()
                    yield last

    def nextSuccessor(self, path):
        """
        Given a path from parent to node,
        find next successor and record its path.
        path of format: [(root, False), (root.left, False),
                            (root.left.right, True)]
        """
        if path[-1][0].right is not None:
            node = path[-1][0].right
            path.append((node, True))
            while node.left is not None:
                node = node.left
                path.append((node, False))
            return
        # find the last right ancestor
        # the last time we took a left
        node, took_right = path.pop()
        while path and took_right:
            node, took_right = path.pop()
        return

    def nextPredecessor(self, path):
        """
        Given a path from parent to node,
        find next Predecessor and record its path.
        """
        if path[-1][0].left is not None:
            node = path[-1][0].left
            path.append((node, False))
            while node.right is not None:
                node = node.right
                path.append((node, True))
            return
        # find the last left ancestor
        # the last time we took a right
        node, took_right = path.pop()
        while path and not took_right:
            node, took_right = path.pop()
        return
