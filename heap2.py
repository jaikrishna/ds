#!/usr/bin/env python3

from collections import MutableSequence

class HeapNode:
    def __init__(self, obj, priority):
        self.obj = obj
        self.heap_key = priority
        self.heap_index = None

    def __repr__(self):
        return '<HeapNode %d (%r)>' % (self.heap_key, self.obj)

class MinHeap(MutableSequence):
    def __init__(self, data=None):
        super().__init__()
        self.nodes = [None]
        if data is not None:
            for n in data:
                self.heap_insert(n)

    def insert(self, _2, _):
        raise NotImplementedError

    def heap_insert(self, heapnode):
        if not isinstance(heapnode, HeapNode):
            raise TypeError
        heapnode.heap_index = len(self.nodes)
        self.nodes.append(heapnode)
        self._heapify_up(heapnode.heap_index)

    def decrease_key(self, index, new_priority):
        node = self.nodes[index]
        if new_priority > node.heap_key:
            raise ValueError('new priority must be lesser')
        node.heap_key = new_priority
        self._heapify_up(index)

    def _heapify_up(self, idx):
        parent = idx//2
        while ((parent > 0) and
               (self.nodes[idx].heap_key < self.nodes[parent].heap_key)):
            self.nodes[parent], self.nodes[idx] = self.nodes[idx], self.nodes[parent]
            self.nodes[parent].heap_index = parent
            self.nodes[idx].heap_index = idx
            idx = parent
            parent = idx//2

    def heap_pop(self):
        # swap the max item with last. heapify_down
        if len(self.nodes) == 2:  # only last item left
            return self.nodes.pop()
        max_item = self.nodes[1]
        self.nodes[1] = self.nodes.pop()
        self.nodes[1].heap_index = 1
        self._heapify_down(1)
        return max_item

    def _heapify_down(self, idx):
        while 2*idx < len(self.nodes):
            max_child, rc = 2*idx, 2*idx+1
            if ((rc < len(self.nodes)) and 
                (self.nodes[rc].heap_key < self.nodes[max_child].heap_key)):
                max_child = rc
            if self.nodes[max_child].heap_key < self.nodes[idx].heap_key:
                self.nodes[max_child], self.nodes[idx] = self.nodes[idx], self.nodes[max_child]
                self.nodes[max_child].heap_index = max_child
                self.nodes[idx].heap_index = idx
                idx = max_child
            else:
                break

    def heap_peek(self):
        return self.nodes[1]

    def __getitem__(self, key):
        return self.nodes[key + 1]

    def __setitem__(self, key, value):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def __iter__(self):
        return iter(self.nodes)

    def __len__(self):
        return len(self.nodes) - 1

    def __repr__(self):
        return repr(self.nodes[1:])


class MaxHeap(MutableSequence):
    def __init__(self, data=None):
        super().__init__()
        self.nodes = [None]
        if data is not None:
            for n in data:
                self.heap_insert(n)

    def insert(self, _2, _):
        raise NotImplementedError

    def heap_insert(self, heapnode):
        if not isinstance(heapnode, HeapNode):
            raise TypeError
        heapnode.heap_index = len(self.nodes)
        self.nodes.append(heapnode)
        self._heapify_up(heapnode.heap_index)

    def _heapify_up(self, idx):
        parent = idx//2
        while ((parent > 0) and
               (self.nodes[idx].heap_key > self.nodes[parent].heap_key)):
            self.nodes[parent], self.nodes[idx] = self.nodes[idx], self.nodes[parent]
            self.nodes[parent].heap_index = parent
            self.nodes[idx].heap_index = idx
            idx = parent
            parent = idx//2

    def heap_pop(self):
        # swap the max item with last. heapify_down
        if len(self.nodes) == 2:  # only last item left
            return self.nodes.pop()
        max_item = self.nodes[1]
        self.nodes[1] = self.nodes.pop()
        self.nodes[1].heap_index = 1
        self._heapify_down(1)
        return max_item

    def _heapify_down(self, idx):
        while 2*idx < len(self.nodes):
            max_child, rc = 2*idx, 2*idx+1
            if ((rc < len(self.nodes)) and 
                (self.nodes[rc].heap_key > self.nodes[max_child].heap_key)):
                max_child = rc
            if self.nodes[max_child].heap_key > self.nodes[idx].heap_key:
                self.nodes[max_child], self.nodes[idx] = self.nodes[idx], self.nodes[max_child]
                self.nodes[max_child].heap_index = max_child
                self.nodes[idx].heap_index = idx
                idx = max_child
            else:
                break

    def heap_peek(self):
        return self.nodes[1]

    def __getitem__(self, key):
        return self.nodes[key + 1]

    def __setitem__(self, key, value):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def __iter__(self):
        return iter(self.nodes)

    def __len__(self):
        return len(self.nodes) - 1

    def __repr__(self):
        return repr(self.nodes[1:])


if __name__ == '__main__':
    h = MinHeap()
    for i in ((-10, -2, -8, 2)):
        h.heap_insert(HeapNode(str(i), i))
        print(h)
    print(h.heap_pop())
    print(h)
    h.heap_insert(HeapNode('-10', -10))
