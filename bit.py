

class BinaryIndexedTree(object):
    __slots__ = ('tree',)
    """
    Binary Indexed Tree supports O(log n) operations for 
    setting frequency at an index and getting cumulative freq
    at index.
    The tree is comprised of number of counters initialized to 0.
    """

    def __init__(self, n):
        """
        The index 0 will not be used & it is illegal to 
        get/update(0)
        """
        self.tree = [0] * (n+1)

    def update(self, idx, val):
        """
        Update the child node and follow the path of
        responsibility upwards until we exceed the size of tree.
        Find the rightmost set bit & add to idx
        """
        while idx < len(self.tree):
            self.tree[idx] += val
            idx += idx & -idx

    def get(self, idx):
        """
        Get the cumulative freq at idx, by following down from 
        child to all parents < child.
        Find the rightmost set bit & remove from idx
        """
        ret = 0
        while idx > 0:
            ret += self.tree[idx]
            idx -= idx & -idx
        return ret

    def get_only(self, idx):
        ret = self.tree[idx]
        assert(idx > 0)
        pidx = idx - (idx & -idx)
        idx -= 1
        while idx > pidx:
            ret -= self.tree[idx]
            idx -= idx & -idx
        return ret

    def find(self, freq):
        pass
