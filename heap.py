#!/usr/bin/env python3.4

class MinHeap(object):
    def __init__(self, getKey, setKey):
        self.h = []
        self.getKey = getKey # lambda i: i.val
        self.setKey = setKey # lambda i,v: setVal(i, v)
        self.items = {}

    def get(self, i):
        return self.getKey(self.h[i])

    def swap(self, i, j):
        self.h[i], self.h[j] = self.h[j], self.h[i]
        self.items[self.h[i]] = i
        self.items[self.h[j]] = j

    def insert(self, val):
        if val in self.items:
            raise ValueError('Item already present in heap')
        self.h.append(val)
        i = len(self.h) - 1
        self.items[val] = i
        p = (i-1)//2
        while p >= 0:
            if self.get(p) > self.get(i):
                self.swap(p, i)
                i = p
                p = (i-1)//2
            else:
                break

    def maxchild(self, i):
        sec = i*2 + 2
        if sec < len(self.h): # both children
            return min((sec-1, self.get(sec-1)),
                       (sec, self.get(sec)),
                       key = lambda y:y[1])
        if sec == len(self.h): # one child
            return (sec-1, self.get(sec-1))
        return None

    def decreaseKey(self, val, newVal):
        if val not in self.items:
            raise KeyError('No such key known')
        i = self.items[val]
        if newVal > self.get(i):
            raise ValueError('Cannot decrease key with negative')
        self.setKey(val, newVal)
        p = (i-1)//2
        while p >= 0:
            if self.get(p) > self.get(i):
                self.swap(p, i)
                i = p
                p = (i-1)//2
            else:
                break

    def pop(self):
        if len(self.h) == 0:
            raise KeyError('No more items in heap')
        ret = self.h[0]
        del self.items[ret]
        if len(self.h) == 1:
            self.h.pop()
            return ret
        self.h[0] = self.h.pop()
        self.items[self.h[0]] = 0
        i = 0
        mc = self.maxchild(i)
        while mc and mc[1] < self.get(i):
            self.swap(mc[0], i)
            i = mc[0]
            mc = self.maxchild(i)
        return ret

class MaxHeap(object):
    def __init__(self, getKey, setKey):
        self.h = []
        self.getKey = getKey  # lambda i: i.val
        self.setKey = setKey  # lambda i,v: setVal(i, v)
        self.items = {}  # {<object>: <index>}

    def get(self, i):
        return self.getKey(self.h[i])

    def swap(self, i, j):
        self.h[i], self.h[j] = self.h[j], self.h[i]
        self.items[self.h[i]] = i
        self.items[self.h[j]] = j

    def insert(self, val):
        if val in self.items:
            raise ValueError('Item already present in heap')
        self.h.append(val)
        i = len(self.h) - 1
        self.items[val] = i
        p = (i-1)//2
        while p >= 0:
            if self.get(p) < self.get(i):
                self.swap(p, i)
                i = p
                p = (i-1)//2
            else:
                break

    def maxchild(self, i):
        return max(((ind, self.get(ind))
                    for ind in (i*2 + 1, i*2 + 2)
                    if ind < len(self.h)),
                   default = None,
                   key = lambda y:y[1])

    def increaseKey(self, val, newVal):
        if val not in self.items:
            raise KeyError('No such key known')
        i = self.items[val]
        if newVal < self.get(i):
            raise ValueError('Cannot increase key with negative')
        self.setKey(val, newVal)
        p = (i-1)//2
        while p >= 0:
            if self.get(p) < self.get(i):
                self.swap(p, i)
                i = p
                p = (i-1)//2
            else:
                break

    def pop(self):
        if len(self.h) == 0:
            raise KeyError('No more items in heap')
        ret = self.h[0]
        del self.items[ret]
        if len(self.h) == 1:
            self.h.pop()
            return ret
        self.h[0] = self.h.pop()
        self.items[self.h[0]] = 0
        i = 0
        mc = self.maxchild(i)
        while mc and mc[1] > self.get(i):
            self.swap(mc[0], i)
            i = mc[0]
            mc = self.maxchild(i)
        return ret
