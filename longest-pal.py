
def longest_pal(s):
    memo = {}

    def dp(i, j):
        entry = (i, j)
        if entry in memo:
            return memo[entry]
        if j - i < 2:
            memo[entry] = entry
            return entry
        inner_sub = (i + 1, j - 1)
        currmax = dp(*inner_sub)
        if currmax == inner_sub and s[i] == s[j-1]:
            currmax = entry
        else:
            currmax = max(currmax, dp(i + 1, j), dp(i, j - 1),
                          key=lambda x: x[1] - x[0])
        memo[entry] = currmax
        return currmax
    res = dp(0, len(s))
    return s[res[0]:res[1]]
