
class UnionFindNaive:
    def __init__(self, size):
        self.parent = [-1] * size
        self.sz = [0] * size

    def makeSet(self, i):
        if self.parent[i] != -1:
            return -1
        self.parent[i] = i
        self.sz[i] = 1
        return i
        
    def findSet(self, i):
        while self.parent[i] != i:
            i = self.parent[i]
        return i

    def compressSet(self, i):
        path = []
        while self.parent[i] != i:
            path.append(i)
            i = self.parent[i]
        for j in path:
            self.parent[j] = i
        return i

    def sameSet(self, i, j):
        return self.findSet(i) == self.findSet(j)

    def _union_naive(self, i, j):
        rooti, rootj = self.findSet(i), self.findSet(j)
        self.parent[rootj] = rooti
        # tree becomes too tall

    def union_size(self, i, j):
        rooti, rootj = self.compressSet(i), self.compressSet(j)
        if self.sz[rooti] < self.sz[rootj]:  # make sz[rooti] larger
            rooti, rootj = rootj, rooti
        self.parent[rootj] = rooti
        self.sz[rooti] += self.sz[rootj]


class UnionFind:
    __slots__ = ('parent', 'rank')

    def __init__(self, size, ready_init=False):
        """
        Given a size initialize structures.

        :param size: The maximum number of sets
        :param ready_init: If True, initialize all sets to contain themselves.
        """
        self.parent = [-1] * size
        self.rank = [0] * size

    def makeSet(self, i):
        """
        Given a set id, create and initialize set to contain itself.

        :param i: The set ID to initialize
        :return: True if success, False if already initialized
        """
        if self.parent[i] != -1:
            return False
        self.parent[i] = i
        return True

    def findSet(self, i):
        """
        Given a set ID, find the root set ID that contains this set.

        :param i: The set ID to search for
        """
        def compress(idx):
            if self.parent[idx] != idx:
                self.parent[idx] = compress(self.parent[idx])
            return self.parent[idx]

        if self.parent[i] == -1:
            return None
        return compress(idx)

    def sameSet(self, i, j):
        return self.findSet(i) == self.findSet(j)

    def union_rank(self, i, j):
        """
        Given two set IDs, join them together into one set.

        :param i,j: The set IDs to join

        :return: True if successful, False if they're already in the same set
        """
        rooti, rootj = self.findSet(i), self.findSet(j)
        if rooti == rootj:
            return False
        # Find parent & compress set
        if self.rank[rooti] < self.rank[rootj]:
            self.parent[rooti] = rootj
        else:
            # increase rank for rooti of both are same
            if self.rank[rooti] == self.rank[rootj]:
                self.rank[rooti] += 1
            self.parent[rootj] = rooti
        return True

class DSU:
    __slots__ = 'parent', 'rank'
    def __init__(self, size):
        self.parent = list(range(size))
        self.rank = [0] * size

    def findSet(self, idx):
        if self.parent[idx] != idx:
            self.parent[idx] = self.findSet(self.parent[idx])
        return self.parent[idx]

    def union(self, nodeA, nodeB):
        rootA, rootB = self.findSet(nodeA), self.findSet(nodeB)
        if rootA == rootB:
            return False
        if self.rank[rootA] < self.rank[rootB]:
            rootA, rootB = rootB, rootA
        elif self.rank[rootA] == self.rank[rootB]:
            self.rank[rootA] += 1
        self.parent[rootB] = rootA
        return True
