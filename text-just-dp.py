#!/usr/bin/env python3


def text_just(wordlen, PG):
    memo = {len(wordlen): 0}
    end = {}
    def dp(i):
        if i in memo:
            print('returning', i, 'early')
            return memo[i]
        print('doing', i)
        currlen = -1
        currmin = 10**10
        mini = i
        for j in range(i, len(wordlen)):
            currlen += wordlen[j] + 1
            if currlen > PG:
                break
            print('trying', j+1)
            cost = (PG - currlen) ** 3 + dp(j+1)
            if cost < currmin:
                mini, currmin = j+1, cost
        end[i] = mini
        memo[i] = currmin
        print('end', i)
        return currmin
    return dp(0), memo, end

sent = 'this is a sentence made of wood'
wordlen = list(map(len, sent.split()))
print(text_just(wordlen, 16))
