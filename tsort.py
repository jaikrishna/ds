class TopSort(object):

    def __init__(self, deps=None):
        self.deps = {} if deps is None else deps

    def add(self, node, dep):
        self.deps.setdefault(node, []).append(dep)

    def kahn(self):
        # calculate In-Degree of every node
        indeg = {n: 0 for n in self.deps.keys()}
        for deplist in self.deps.values():
            for dep in deplist:
                indeg[dep] = indeg.get(dep, 0) + 1

        # start building top-order with 0 degree nodes
        top = [n for n, deg in indeg.items() if deg == 0]

        # simulate removing them from graph in top-order
        i = 0
        while i < len(top):
            for dep in self.deps.get(top[i], []):
                indeg[dep] -= 1
                if indeg[dep] == 0:
                    top.append(dep)
            i += 1

        # check for errors
        if len(top) != len(indeg):
            raise ValueError('Cyclic deps')
        return top

    def simple_tsort(self, indegree, depends):
        to_proc = [i for i in range(len(indegree)) if indegree[i] == 0]
        if not to_proc:
            raise ValueError('nothing to start with')
        ret = []
        while to_proc:
            node = to_proc.pop()
            for dep in depends[node]:
                indegree[dep] -= 1
                if indegree[dep] == 0:
                    to_proc.append(dep)
            ret.append(node)
        if len(ret) != len(depends):
            raise ValueError('cannot remove all', ret)
        return ret


def main():
    words = ["wrt","wrf","er","ett","rftt"]
    chars = set()
    for w in words:  # to handle cases like [z, z] [ab, adc]
        chars.update(w)
    deps = {}
    for i in range(len(words)-1):
        for a, b in zip(words[i], words[i+1]):
            if a != b:
                deps.setdefault(a, []).append(b)
                break
    try:
        top = ''.join(TopSort(deps).kahn())
    except ValueError:
        return ''
    chars.difference_update(top)
    return top + ''.join(chars)
