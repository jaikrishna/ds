
def matrix_mul(matsize):
    memo = {}
    path = {}
    def cost(i, j, k):
        return matsize[i][0] * matsize[k][0] * matsize[j-1][1]
        
    def dp(i, j):
        if j - i == 1:
            return 0
        if (i, j) in memo:
            return memo[(i, j)]
        r, ret = min(enumerate((dp(i, k) + dp(k, j) + cost(i, j, k)
                                for k in range(i+1, j)), i+1),
                    key=lambda x: x[1])
        memo[(i, j)] = ret
        path[(i, j)] = r
        return ret
    return dp(0, len(matsize)), memo, path
