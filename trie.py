
class TrieNode:
    __slots__ = ('child', 'complete')

    def __init__(self):
        self.child = [None]*26
        self.complete = False

str_to_ord = lambda c: ord(c) - 97

class Trie:
    __slots__ = ('root',)
    
    def __init__(self):
        self.root = TrieNode()

    def addstr(self, word):
        return self.add(map(str_to_ord, word))

    def add(self, word):
        node = self.root
        for c in word:
            if node.child[c] is None:
                node.child[c] = TrieNode()
            node = node.child[c]
        node.complete = True
        return node

    def searchstr(self, word):
        return self.search(map(str_to_ord, word))

    def search(self, word):
        node = self.root
        path = []
        for c in word:
            if node.child[c] is None:
                break
            node = node.child[c]
            path.append(c)
        return path, node
