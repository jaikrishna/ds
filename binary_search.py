

def search_left(val, nums):
    "Find leftmost val in nums"
    i, j = 0, len(nums)
    while i < j:
        mid = (i + j) // 2
        if nums[mid] < val:
            i = mid + 1
        else:  # nums[mid] >= val
            j = mid  # we'll make i reach this
    return i

def search_right(val, nums):
    "Find rightmost val in nums"
    i, j = 0, len(nums)
    while i < j:
        mid = (i + j) // 2
        if nums[mid] > val:
            j = mid
        else:  # nums[mid] <= val
            i = mid + 1
    # i reaches a point next to last occurence of val
    if nums[i - 1] == val:
        i -= 1
    # now can use .insert
    return i
