#!/usr/bin/env python3.4

from heap2 import MinHeap, HeapNode

class Graph:
    def __init__(self):
        self.nodes = {} # {nodeid: Node}
        self.edges = {} # {nodeid: [(distance, nodeid)]}

    def addNode(self, node, links=[]):
        self.nodes[node.obj] = node
        self.edges[node.obj] = links

    def addEdge(self, obj, distance, target):
        self.edges[obj].append((distance, target))


class Dijkstra:
    def __init__(self, nodes):
        self.graph = Graph()
        # {nodeid: [(dist, nodeid)]}
        for n, links in nodes.items():
            self.graph.addNode(HeapNode(n, 10**10), links)

    def shortestPath(self, source_id):
        #Node.setNodeValue(self.graph.nodes[source], 0)
        source = self.graph.nodes[source_id]
        source.heap_key = 0
        h = MinHeap(self.graph.nodes.values())
        for numnodes in range(len(self.graph.nodes)):
            source = h.heap_pop()
            sourcedist = source.heap_key
            for dist, dest in self.graph.edges[source.obj]:
                newdist = sourcedist + dist
                if newdist < self.graph.nodes[dest].heap_key:
                    h.decrease_key(self.graph.nodes[dest].heap_index, newdist)
        return self.graph.nodes

if __name__ == '__main__':
    nodes = {
            1: [(3, 3), (6, 4)],
            2: [(1, 4)],
            3: [(5, 4)],
            4: [],
            0: [(2, 1), (6, 2), (7, 3)]
            }

    d = Dijkstra(nodes)
    print(d.shortestPath(0))
