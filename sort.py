
def xmergesort(A, left=0, right=None):
    if right is None:
        right = len(A)
    if right - left < 2:
        return
    
    mid = left + ((right - left) // 2)
    mergesort(A, left, mid)
    mergesort(A, mid, right)

    s = []
    i, j = left, mid
    while i < mid and j < right:
        if A[i] < A[j]:
            s.append(A[i])
            i += 1
        else:
            s.append(A[j])
            j += 1
    s += A[i:mid]
    s += A[j:right]
    A[left:right] = s
    return

def quicksort(A, left, right):
    def partition(first, last):
        if first >= last:
            return
        pivot = A[last]
        i = first - 1
        for j in range(first, last):
            if A[j] < pivot and i != j:
                i += 1
                A[i], A[j] = A[j], A[i]
        i += 1
        A[i], A[last] = A[last], A[i]
        return i

    if left < right:
        pi = partition(left, right)
        quicksort(A, left, pi - 1)
        quicksort(A, pi + 1, right)

def mergesort(A, left, right):
    if right - left < 2:
        return
    mid = (left + right) // 2
    mergesort(A, left, mid)
    mergesort(A, mid, right)
    ret = []
    i, j = left, mid
    while i < mid and j < right:
        if A[i] < A[j]:
            ret.append(A[i])
            i += 1
        else:
            ret.append(A[j])
            j += 1
    A[left:right] = ret + A[i:mid] + A[j:right]

if __name__ == '__main__':
    import random
    for _ in range(random.randrange(100)):
        a = [random.randint(-10000, 10000) 
             for _ in range(random.randrange(1000))]
        mergesort(a, 0, len(a))
        #quicksort(a, 0, len(a) - 1)
        assert a == sorted(a)
