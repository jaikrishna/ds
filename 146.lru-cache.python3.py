#
# [146] LRU Cache
#
# https://leetcode.com/problems/lru-cache/description/
#
# algorithms
# Hard (21.42%)
# Total Accepted:    206.4K
# Total Submissions: 961.1K
# Testcase Example:  '["LRUCache","put","put","get","put","get","put","get","get","get"]\n[[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]'
#
# 
# Design and implement a data structure for Least Recently Used (LRU) cache. It
# should support the following operations: get and put.
# 
# 
# 
# get(key) - Get the value (will always be positive) of the key if the key
# exists in the cache, otherwise return -1.
# put(key, value) - Set or insert the value if the key is not already present.
# When the cache reached its capacity, it should invalidate the least recently
# used item before inserting a new item.
# 
# 
# Follow up:
# Could you do both operations in O(1) time complexity?
# 
# Example:
# 
# LRUCache cache = new LRUCache( 2 /* capacity */ );
# 
# cache.put(1, 1);
# cache.put(2, 2);
# cache.get(1);       // returns 1
# cache.put(3, 3);    // evicts key 2
# cache.get(2);       // returns -1 (not found)
# cache.put(4, 4);    // evicts key 1
# cache.get(1);       // returns -1 (not found)
# cache.get(3);       // returns 3
# cache.get(4);       // returns 4
# 
# 
#
from collections import deque

class Node:
    __slots__ = ('key', 'value', 'next', 'prev')

    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.next = None
        self.prev = None

    def __repr__(self):
        return '<Node %s>' % (self.key,)

class LList:
    __slots__ = ('first', 'last')

    def __init__(self):
        self.first = None
        self.last = None

    def pop_first(self):
        ret = self.first
        if ret is None:
            raise KeyError
        self.first = self.first.next
        if self.first is not None:
            self.first.prev = None
        else:
            self.last = None
        return ret

    def move_to_last(self, node):
        if node is self.last:
            return
        node.next.prev = node.prev
        if node is self.first:
            self.first = node.next
        else:
            node.prev.next = node.next
        # node is detached
        self.last.next = node
        node.prev = self.last
        self.last = node
        node.next = None

    def add_to_last(self, node):
        if self.last is not None:
            self.last.next = node
            node.prev = self.last
            self.last = node
        else:
            self.last = node
            self.first = node


class LRUCache:

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.nodes = {}
        self.ll = LList()
        self.cap = capacity
        

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        node = self.nodes.get(key)
        if node is not None:
            self.ll.move_to_last(node)
            return node.value
        return -1
        

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        node = self.nodes.get(key)
        if node is not None:  # already existed
            node.value = value
            self.ll.move_to_last(node)
        else:
            if len(self.nodes) == self.cap:
                del self.nodes[self.ll.pop_first().key]
            node = Node(key, value)
            self.nodes[key] = node
            self.ll.add_to_last(node)

        


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
