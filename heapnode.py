
class Node:
    def __init__(self, node_id, prio):
        self.node_id = node_id
        self.prio = prio

    def __lt__(self, other):
        return self.prio < other.prio

    def __le__(self, other):
        return self.prio <= other.prio

    def __gt__(self, other):
        return self.prio > other.prio

    def __ge__(self, other):
        return self.prio >= other.prio

    def __eq__(self, other):
        return self.prio == other.prio

    def __ne__(self, other):
        return self.prio != other.prio

    def __repr__(self):
        return '<Node {}: {}>'.format(self.node_id, self.prio)
