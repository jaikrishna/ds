
def solution(number):
    # Type your solution here 
    """
    1    I
    5    V
    10   X
    50   L
    100  C
    500  D
    1000 M
    """
    roman_value = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    roman_repr  = ['M', 'CM', 'D', 'CD', 'C','XC', 'L', 'XL', 'X', 
                   'IX', 'V', 'IV', 'I']
    assert len(roman_value) == len(roman_repr)
    i = 0
    res = []
    while number > 0:  # iterate with number while it has value
        # while a roman_value can be subtracted, stay in same index
        if number >= roman_value[i]:
            number -= roman_value[i]   
            res.append(roman_repr[i])
        else:  # number is smaller, try next index
            i += 1
    return ''.join(res)
